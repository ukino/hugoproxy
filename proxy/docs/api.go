package docs

import (
	auth "gitlab.com/ukino/hugoproxy/proxy/internal/modules/auth/controller"
	geo "gitlab.com/ukino/hugoproxy/proxy/internal/modules/geoservice/controller"
	user "gitlab.com/ukino/hugoproxy/proxy/internal/modules/user/controller"
)

//go:generate swagger generate spec -o ../static/swagger.yml --scan-models

// swagger:route GET /api/users/{id} getUser getUserRequest
// Получение пользователя из базы данных.
// responses:
//   200: getUserResponse
//	 Security:
//    - jwt: []

// swagger:parameters getUserRequest
type getUserRequest struct {
	// in:body
	Body user.GetUserRequest
}

// swagger:response getUserResponse
type getUserResponse struct {
	// in:body
	Body user.GetUserResponse
}

// swagger:route POST /api/users/add addUser addUserRequest
// Добавление пользователя в базу данных.
// responses:
//   200: addUserResponse
//	 Security:
//    - jwt: []

// swagger:parameters addUserRequest
type addUserRequest struct {
	// in:body
	Body user.AddUserRequest
}

// swagger:response addUserResponse
type addUserResponse struct {
	// in:body
	Body user.AddUserResponse
}

// swagger:route POST /api/users/update updateUser updateUserRequest
// Изменение пользователя в базе данных.
// responses:
//   200: updateUserResponse
//	 Security:
//    - jwt: []

// swagger:parameters updateUserRequest
type updateUserRequest struct {
	// in:body
	Body user.UpdateUserRequest
}

// swagger:response updateUserResponse
type updateUserResponse struct {
	// in:body
	Body user.UpdateUserResponse
}

// swagger:route POST /api/users/delete deleteUser deleteUserRequest
// Пометка пользователя на удаление в базе данных.
// responses:
//   200: deleteUserResponse
//	 Security:
//    - jwt: []

// swagger:parameters deleteUserRequest
type deleteUserRequest struct {
	// in:body
	Body user.DeleteUserRequest
}

// swagger:response deleteUserResponse
type deleteUserResponse struct {
	// in:body
	Body user.DeleteUserResponse
}

// swagger:route GET /api/users/list listUser listUserRequest
// Получение пользователей из базы данных по определенным условиям.
// responses:
//   200: listUserResponse
//	 Security:
//    - jwt: []

// swagger:parameters listUserRequest
type listUserRequest struct {
	// in:body
	Body user.ListUserRequest
}

// swagger:response listUserResponse
type listUserResponse struct {
	// in:body
	Body user.ListUserResponse
}

// swagger:route POST /api/address/geocode geocode geocodeRequest
// Поиск локации по Ширине и Долготе.
// responses:
//   200: geocodeResponse
//	 Security:
//    - jwt: []

// swagger:parameters geocodeRequest
type geocodeRequest struct {
	// in:body
	Body geo.GeocodeRequest
}

// swagger:response geocodeResponse
type geocodeResponse struct {
	// in:body
	Body geo.GeocodeResponse
}

// swagger:route POST /api/address/search search searchRequest
// Поиск локации по IP.
// responses:
//   200: searchResponse
//	 Security:
//    - jwt: []

// swagger:parameters searchRequest
type searchRequest struct {
	// in:body
	Body geo.SearchRequest
}

// swagger:response searchResponse
type searchResponse struct {
	// in:body
	Body geo.SearchResponse
}

// swagger:route POST /api/login login loginRequest
// Вход.
// responses:
//   200: loginResponse

// swagger:parameters loginRequest
type loginRequest struct {
	// in:body
	Body auth.LoginRequest
}

// swagger:response loginResponse
type loginResponse struct {
	// in:body
	Body auth.LoginResponse
}

// swagger:route POST /api/register register registerRequest
// Регистрация.
// responses:
//   200: registerResponse
// 	 401: description:failed

// swagger:parameters registerRequest
type registerRequest struct {
	// in:body
	Body auth.RegisterRequest
}

// swagger:response registerResponse
type registerResponse struct {
	// in:body
	Body auth.RegisterResponse
}
