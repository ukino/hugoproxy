package graph

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

type Node struct {
	ID    int
	Name  string
	Form  string // "circle", "rect", "square", "ellipse", "round-rect", "rhombus"
	Links []*Node
}

func Worker() {
	ticker := time.NewTicker(5 * time.Second)
	for now := range ticker.C {
		_ = now
		file, err := os.Create("./app/static/tasks/graph.md")
		if err != nil {
			return
		}
		js := toMermaidJS(CreateGraph())
		str := fmt.Sprintf("---\nmenu:\n    after:\n        name: graph\n        weight: 1\ntitle: Построение графа\n---\n\n# Построение графа\n\nНужно написать воркер, который будет строить граф на текущей странице, каждые 5 секунд\nОт 5 до 30 элементов, случайным образом. Все ноды графа должны быть связаны.\n\n```go\ntype Node struct {\n    ID int\n    Name string\n\tForm string // \"circle\", \"rect\", \"square\", \"ellipse\", \"round-rect\", \"rhombus\"\n    Links []*Node\n}\n```\n\n## Mermaid Chart\n\n[MermaidJS](https://mermaid-js.github.io/) is library for generating svg charts and diagrams from text.\n\n## Пример\n\n{{< columns >}}\n```tpl\n{{</*/* mermaid [class=\"text-center\"]*/*/>}}\ngraph LR\nA[Square Rect] --> B((Circle))\nA --> C(Round Rect)\nB --> D{Rhombus}\nC --> D\nC --> B\n{{</*/* /mermaid */*/>}}\n```\n\n<--->\n\n{{< mermaid >}}\ngraph LR\nA[Square Rect] --> B((Circle))\nA --> C(Round Rect)\nB --> D{Rhombus}\nC --> D\nC --> B\n{{< /mermaid >}}\n\n{{< /columns >}}\n\n{{< mermaid >}}\n%s\n{{< /mermaid >}}", js)
		_, err = file.WriteString(str)
		if err != nil {
			return
		}
	}

}

func RandomForm() string {
	forms := []string{"((circle))", "[rect]", "[[square]]", "(ellipse)", "(round-rect)", "{rhombus}"}
	return forms[rand.Intn(len(forms))]
}

func CreateGraph() []*Node {
	rand.Seed(time.Now().UnixNano())

	n := rand.Intn(30)
	nodes := make([]*Node, n)
	for i := 0; i < n; i++ {
		nodes[i] = &Node{
			ID:   i,
			Name: fmt.Sprintf("Node%d", i),
			Form: RandomForm(),
		}
	}

	for i := 0; i < n; i++ {
		if i+1 < n {
			nodes[i].Links = append(nodes[i].Links, nodes[i+1])
		}
		if i+2 < n {
			nodes[i].Links = append(nodes[i].Links, nodes[i+2])
		}
	}
	return nodes
}

func toMermaidJS(Nodes []*Node) string {
	mermaid := "graph LR\n"
	for _, node := range Nodes {
		mermaid += fmt.Sprintf("%s%s\n", node.Name, node.Form)
		for _, link := range node.Links {
			mermaid += fmt.Sprintf("%s --> %s\n", node.Name, link.Name)
		}
	}
	return mermaid
}
