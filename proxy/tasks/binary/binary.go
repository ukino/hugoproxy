package binary

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"
)

type Node struct {
	Key    int
	Height int
	Left   *Node
	Right  *Node
}

type AVLTree struct {
	Root *Node
}

func NewNode(key int) *Node {
	return &Node{Key: key, Height: 1}
}

func (t *AVLTree) Insert(key int) {
	t.Root = insert(t.Root, key)
}

func (t *AVLTree) ToMermaid() string {
	return toMermaid(t.Root, "")
}

func toMermaid(node *Node, parent string) string {
	if node == nil {
		return ""
	}
	result := ""
	if parent != "" {
		result = fmt.Sprintf("%s --> %d\n", parent, node.Key)
	}
	result += toMermaid(node.Left, fmt.Sprintf("%d", node.Key))
	result += toMermaid(node.Right, fmt.Sprintf("%d", node.Key))
	return result
}

func height(node *Node) int {
	if node == nil {
		return 0
	}
	return node.Height
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func updateHeight(node *Node) {
	node.Height = max(height(node.Left), height(node.Right)) + 1
}

func getBalance(node *Node) int {
	if node == nil {
		return 0
	}
	return height(node.Left) - height(node.Right)
}

func leftRotate(x *Node) *Node {
	y := x.Right
	T2 := y.Left
	y.Left = x
	x.Right = T2
	updateHeight(x)
	updateHeight(y)
	return y
}

func rightRotate(y *Node) *Node {
	x := y.Left
	T2 := x.Right
	x.Right = y
	y.Left = T2
	updateHeight(y)
	updateHeight(x)
	return x
}

func insert(node *Node, key int) *Node {
	if node == nil {
		return NewNode(key)
	}
	if key < node.Key {
		node.Left = insert(node.Left, key)
	} else if key > node.Key {
		node.Right = insert(node.Right, key)
	} else {
		return node
	}
	updateHeight(node)
	balance := getBalance(node)
	if balance > 1 {
		if key < node.Left.Key {
			return rightRotate(node)
		} else {
			node.Left = leftRotate(node.Left)
			return rightRotate(node)
		}
	}
	if balance < -1 {
		if key > node.Right.Key {
			return leftRotate(node)
		} else {
			node.Right = rightRotate(node.Right)
			return leftRotate(node)
		}
	}
	return node
}

func GenerateTree(count int) {
	ticker := time.NewTicker(5 * time.Second)
	tree := &AVLTree{}
	counter := 0
	for range ticker.C {
		tree.Insert(rand.Intn(1000))
		counter++

		file, err := os.Create("/app/static/tasks/binary.md")
		if err != nil {
			log.Println(err)
			return
		}
		str := fmt.Sprintf("---\nmenu:\n    after:\n        name: binary_tree\n        weight: 2\ntitle: Построение сбалансированного бинарного дерева\n---\n\n# Задача построить сбалансированное бинарное дерево\nИспользуя AVL дерево, постройте сбалансированное бинарное дерево, на текущей странице.\n\nНужно написать воркер, который стартует дерево с 5 элементов, и каждые 5 секунд добавляет новый элемент в дерево.\n\nКаждые 5 секунд на странице появляется актуальная версия, сбалансированного дерева.\n\nПри вставке нового элемента, в дерево, нужно перестраивать дерево, чтобы оно оставалось сбалансированным.\n\nКак только дерево достигнет 100 элементов, генерируется новое дерево с 5 элементами.\n\n```go\npackage binary\n\nimport (\n\t\"fmt\"\n\t\"math/rand\"\n\t\"time\"\n)\n\ntype Node struct {\n\tKey    int\n\tHeight int\n\tLeft   *Node\n\tRight  *Node\n}\n\ntype AVLTree struct {\n\tRoot *Node\n}\n\nfunc NewNode(key int) *Node {\n\treturn &Node{Key: key, Height: 1}\n}\n\nfunc (t *AVLTree) Insert(key int) {\n\tt.Root = insert(t.Root, key)\n}\n\nfunc (t *AVLTree) ToMermaid() string {\n\n}\n\nfunc height(node *Node) int {\n\n}\n\nfunc max(a, b int) int {\n\n}\n\nfunc updateHeight(node *Node) {\n\n}\n\nfunc getBalance(node *Node) int {\n\n}\n\nfunc leftRotate(x *Node) *Node {\n\n}\n\nfunc rightRotate(y *Node) *Node {\n\n}\n\nfunc insert(node *Node, key int) *Node {\n\n}\n\nfunc GenerateTree(count int) *AVLTree {\n\n}\n```\n\nНе обязательно использовать выше описанный код, можно использовать любую реализацию, выдающую сбалансированное бинарное дерево.\n\n## Mermaid Chart\n\n[MermaidJS](https://mermaid-js.github.io/) is library for generating svg charts and diagrams from text.\n\n## Пример вывода\n\n{{< columns >}}\n```tpl\n{{</*/* mermaid [class=\"text-center\"]*/*/>}}\ngraph TD\n610 --> 176\n176 --> 65\n65 --> 48\n48 --> 4\n65 --> 130\n176 --> 340\n340 --> 273\n273 --> 220\n273 --> 317\n340 --> 348\n348 --> 403\n610 --> 758\n758 --> 703\n703 --> 684\n684 --> 673\n703 --> 741\n741 --> 705\n758 --> 949\n949 --> 847\n{{</*/* /mermaid */*/>}}\n```\n\n{{< /columns >}}\n\n{{< mermaid >}}\ngraph TD\n610 --> 176\n176 --> 65\n65 --> 48\n48 --> 4\n65 --> 130\n176 --> 340\n340 --> 273\n273 --> 220\n273 --> 317\n340 --> 348\n348 --> 403\n610 --> 758\n758 --> 703\n703 --> 684\n684 --> 673\n703 --> 741\n741 --> 705\n758 --> 949\n949 --> 847\n\n{{< /mermaid >}}\n\n{{< mermaid >}}\ngraph TD\n%s{{< /mermaid >}}\n", tree.ToMermaid())
		_, err = file.WriteString(str)
		if err != nil {
			return
		}
		if counter == count {
			break
		}
	}
}

func Worker() {
	for {
		GenerateTree(100)
	}
}
