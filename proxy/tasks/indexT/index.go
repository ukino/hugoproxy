package indexT

import (
	"fmt"
	"os"
	"time"
)

func Worker() {
	counter := 0
	ticker := time.NewTicker(5 * time.Second)
	for now := range ticker.C {
		file, err := os.Create("/app/static/tasks/_index.md")
		if err != nil {
			return
		}
		counter++
		str := fmt.Sprintf("---\nmenu:\n    before:\n        name: tasks\n        weight: 5\ntitle: Обновление данных в реальном времени\n---\n\n# Задача: Обновление данных в реальном времени\n\nНапишите воркер, который будет обновлять данные в реальном времени, на текущей странице.\nТекст данной задачи менять нельзя, только время и счетчик.\n\nФайл данной страницы: `/app/static/tasks/_index.md`\n\nДолжен меняться счетчик и время:\n\nТекущее время: %s\n\nСчетчик: %d\n\n## Критерии приемки:\n- [ ] Воркер должен обновлять данные каждые 5 секунд\n- [ ] Счетчик должен увеличиваться на 1 каждые 5 секунд\n- [ ] Время должно обновляться каждые 5 секунд", now.Format("2006-01-02 15:04:05"), counter)
		_, err = file.WriteString(str)
		if err != nil {
			return

		}
	}
}
