package config

import (
	"os"
	"strconv"
	"time"
)

const (
	AppName = "APP_NAME"

	serverPort = "SERVER_PORT"
)

//go:generate easytags $GOFILE yaml

type AppConf struct {
	AppName string `yaml:"app_name"`
	Server  Server `yaml:"server"`
	DB      DB     `yaml:"db"`
}

type DB struct {
	Net      string `yaml:"net"`
	Driver   string `yaml:"driver"`
	Name     string `yaml:"name"`
	User     string `json:"-" yaml:"user"`
	Password string `json:"-" yaml:"password"`
	Host     string `yaml:"host"`
	MaxConn  int    `yaml:"max_conn"`
	Port     string `yaml:"port"`
	Timeout  int    `yaml:"timeout"`
}

func NewAppConf() AppConf {
	port := os.Getenv(serverPort)
	maxconn, _ := strconv.Atoi(os.Getenv("MAX_CONN"))
	timeout, _ := strconv.Atoi(os.Getenv("DB_TIMEOUT"))

	return AppConf{
		AppName: os.Getenv(AppName),
		Server: Server{
			Port: port,
		},
		DB: DB{
			Net:      os.Getenv("DB_NET"),
			Driver:   os.Getenv("DB_DRIVER"),
			Name:     os.Getenv("DB_NAME"),
			User:     os.Getenv("DB_USER"),
			Password: os.Getenv("DB_PASSWORD"),
			Host:     os.Getenv("DB_HOST"),
			MaxConn:  maxconn,
			Port:     os.Getenv("DB_PORT"),
			Timeout:  timeout,
		},
	}
}

type Server struct {
	Port            string        `yaml:"port"`
	ShutdownTimeout time.Duration `yaml:"shutdown_timeout"`
}
