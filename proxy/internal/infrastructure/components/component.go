package components

import (
	"github.com/go-chi/jwtauth/v5"
	"github.com/ptflp/godecoder"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/responder"
	"go.uber.org/zap"
)

type Components struct {
	Responder responder.Responder
	Decoder   godecoder.Decoder
	Logger    *zap.Logger
	TokenAuth *jwtauth.JWTAuth
	Users     map[int]string
}

func NewComponents(responder responder.Responder, decoder godecoder.Decoder, logger *zap.Logger, tokenAuth *jwtauth.JWTAuth, users map[int]string) *Components {
	return &Components{
		Responder: responder,
		Decoder:   decoder,
		Logger:    logger,
		TokenAuth: tokenAuth,
		Users:     users,
	}
}
