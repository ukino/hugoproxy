package middleware

import (
	"github.com/stretchr/testify/assert"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewReverseProxy(t *testing.T) {
	type args struct {
		host string
		port string
	}
	tests := []struct {
		name string
		args args
		want *ReverseProxy
	}{
		{
			name: "one",
			args: args{
				host: "hugo",
				port: "1212",
			},
			want: &ReverseProxy{
				host: "hugo",
				port: "1212",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, NewReverseProxy(tt.args.host, tt.args.port))
		})
	}
}

func TestReverseProxy_ReverseProxy(t *testing.T) {
	request, err := http.NewRequest("GET", "http://localhost:8080/api/", nil)
	if err != nil {
		log.Println(err)
		return
	}

	rr := httptest.NewRecorder()
	proxy := NewReverseProxy("hugo", "1313")

	handler := http.HandlerFunc(Handler)
	reverseProxy := proxy.ReverseProxy(handler)
	reverseProxy.ServeHTTP(rr, request)
}

func TestReverseProxy_ReverseProxy2(t *testing.T) {
	request, err := http.NewRequest("GET", "http://localhost:8080/tasks/", nil)
	if err != nil {
		log.Println(err)
		return
	}

	rr := httptest.NewRecorder()
	proxy := NewReverseProxy("hugo", "1313")

	handler := http.HandlerFunc(Handler)
	reverseProxy := proxy.ReverseProxy(handler)
	reverseProxy.ServeHTTP(rr, request)
}
