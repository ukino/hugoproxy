package middleware

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
)

type ReverseProxy struct {
	host string
	port string
}

func NewReverseProxy(host, port string) *ReverseProxy {
	return &ReverseProxy{
		host: host,
		port: port,
	}
}

func (rp *ReverseProxy) ReverseProxy(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.URL)
		if !strings.HasPrefix(r.URL.Path, "/api/") && r.URL.Path != "/static/swagger.yml" && r.URL.Path != "/swagger" {
			log.Println("Try redirect")
			proxy := httputil.NewSingleHostReverseProxy(&url.URL{Scheme: "http", Host: rp.host + ":" + rp.port})
			proxy.ServeHTTP(w, r)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func Handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello from API")
}
