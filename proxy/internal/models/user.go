package models

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

type User struct {
	ID        int    `json:"id" db:"id" db_type:"SERIAL PRIMARY KEY" db_default:"NOT NULL"`
	Name      string `json:"name" db:"name" db_ops:"create,update" db_type:"TEXT"`
	Password  string `json:"password" db:"password" db_ops:"create,update" db_type:"TEXT"`
	IsDeleted bool   `json:"is_deleted" db:"is_deleted" db_ops:"create,update" db_type:"BOOLEAN" db_default:"DEFAULT false"`
}

func (u *User) TableName() string {
	return "users"
}

func (u *User) OnCreate() []string {
	return []string{}
}
