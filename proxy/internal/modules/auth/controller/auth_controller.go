package controller

import (
	"encoding/json"
	"fmt"
	"github.com/ptflp/godecoder"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/components"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/responder"
	"gitlab.com/ukino/hugoproxy/proxy/internal/modules/auth/service"
	"net/http"
)

type Auther interface {
	LoginHandler(w http.ResponseWriter, r *http.Request)
	RegisterHandler(w http.ResponseWriter, r *http.Request)
}

type Auth struct {
	service.Auther
	responder.Responder
	godecoder.Decoder
}

func NewAuth(service service.Auther, components *components.Components) Auther {
	return &Auth{
		Auther:    service,
		Responder: components.Responder,
		Decoder:   components.Decoder,
	}
}

func (a *Auth) LoginHandler(w http.ResponseWriter, r *http.Request) {
	var lr LoginRequest
	err := json.NewDecoder(r.Body).Decode(&lr)
	if err != nil {
		a.Responder.ErrorBadRequest(w, err)
		return
	}
	if ok := a.Auther.Check(lr.Id, lr.Password); !ok {
		a.Responder.ErrorUnauthorized(w, fmt.Errorf("incorrect user"))
		return
	}
	token := a.GetToken(lr.Id)
	response := LoginResponse{Token: token}
	a.Responder.OutputJSON(w, response)
}

func (a *Auth) RegisterHandler(w http.ResponseWriter, r *http.Request) {
	var rr RegisterRequest
	err := json.NewDecoder(r.Body).Decode(&rr)
	if err != nil {
		a.Responder.ErrorBadRequest(w, err)
		return
	}
	successful := a.AddUser(rr.Id, rr.Password)

	response := RegisterResponse{successful}
	a.Responder.OutputJSON(w, response)
}
