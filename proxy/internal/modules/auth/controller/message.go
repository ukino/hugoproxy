package controller

type LoginRequest struct {
	Id       int    `json:"user_id"`
	Password string `json:"user_password"`
}

type LoginResponse struct {
	Token string `json:"token"`
}

type RegisterRequest struct {
	Id       int    `json:"user_id"`
	Password string `json:"user_password"`
}

type RegisterResponse struct {
	Success bool `json:"success"`
}
