package controller

import (
	"bytes"
	"github.com/go-chi/jwtauth/v5"
	"github.com/ptflp/godecoder"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/components"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/responder"
	service2 "gitlab.com/ukino/hugoproxy/proxy/internal/modules/auth/service"
	"go.uber.org/zap"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestLoginHandler(t *testing.T) {
	token := jwtauth.New("HS256", []byte("secret"), nil)
	users := make(map[int]string)
	logger := zap.NewExample()
	decoder := godecoder.NewDecoder()
	Responder := responder.NewResponder(decoder, logger)
	components := components.NewComponents(Responder, decoder, logger, token, users)
	auth := NewAuth(service2.NewAuthService(components), components)

	request, err := http.NewRequest(http.MethodPost, "http://localhost:8080/api/login", bytes.NewBuffer([]byte("{user_id: 1, user_password:sda}")))
	if err != nil {
		return
	}

	rr := httptest.NewRecorder()

	handlerFunc := http.HandlerFunc(auth.LoginHandler)

	handlerFunc.ServeHTTP(rr, request)
}

func TestLoginHandler2(t *testing.T) {
	token := jwtauth.New("HS256", []byte("secret"), nil)
	users := make(map[int]string)
	logger := zap.NewExample()
	decoder := godecoder.NewDecoder()
	Responder := responder.NewResponder(decoder, logger)
	components := components.NewComponents(Responder, decoder, logger, token, users)
	auth := NewAuth(service2.NewAuthService(components), components)

	request, err := http.NewRequest(http.MethodPost, "http://localhost:8080/api/register", bytes.NewBuffer([]byte(`{"user_id": 1, "user_password": "sda"}`)))
	if err != nil {
		return
	}

	rr := httptest.NewRecorder()

	handlerFunc := http.HandlerFunc(auth.LoginHandler)

	handlerFunc.ServeHTTP(rr, request)
}

func TestRegisterHandler(t *testing.T) {
	token := jwtauth.New("HS256", []byte("secret"), nil)
	users := make(map[int]string)
	logger := zap.NewExample()
	decoder := godecoder.NewDecoder()
	Responder := responder.NewResponder(decoder, logger)
	components := components.NewComponents(Responder, decoder, logger, token, users)
	auth := NewAuth(service2.NewAuthService(components), components)

	request, err := http.NewRequest(http.MethodPost, "http://localhost:8080/api/register", bytes.NewBuffer([]byte("{user_id: 1, user_password:sda}")))
	if err != nil {
		return
	}

	rr := httptest.NewRecorder()

	handlerFunc := http.HandlerFunc(auth.RegisterHandler)

	handlerFunc.ServeHTTP(rr, request)
}

func TestRegisterHandler2(t *testing.T) {
	token := jwtauth.New("HS256", []byte("secret"), nil)
	users := make(map[int]string)
	logger := zap.NewExample()
	decoder := godecoder.NewDecoder()
	Responder := responder.NewResponder(decoder, logger)
	components := components.NewComponents(Responder, decoder, logger, token, users)
	auth := NewAuth(service2.NewAuthService(components), components)

	request, err := http.NewRequest(http.MethodPost, "http://localhost:8080/api/register", bytes.NewBuffer([]byte(`{"user_id": 1, "user_password": "sda"}`)))
	if err != nil {
		return
	}

	rr := httptest.NewRecorder()

	handlerFunc := http.HandlerFunc(auth.RegisterHandler)

	handlerFunc.ServeHTTP(rr, request)
}
