package service

import (
	"github.com/go-chi/jwtauth/v5"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/components"
	"go.uber.org/zap"
	"golang.org/x/crypto/bcrypt"
)

type AuthService struct {
	users     map[int]string
	tokenAuth *jwtauth.JWTAuth
	logger    *zap.Logger
}

func NewAuthService(components *components.Components) *AuthService {
	return &AuthService{
		users:     components.Users,
		tokenAuth: components.TokenAuth,
		logger:    components.Logger,
	}
}

func (a *AuthService) Check(id int, password string) bool {
	hashedPassword, ok := a.users[id]
	if !ok || bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)) != nil {
		return false
	}
	return true
}

func (a *AuthService) GetToken(id int) string {
	_, tokenString, _ := a.tokenAuth.Encode(map[string]interface{}{"user_id": id})
	return tokenString
}

func (a *AuthService) AddUser(id int, passwordU string) bool {
	password, _ := bcrypt.GenerateFromPassword([]byte(passwordU), 10)
	a.users[id] = string(password)
	return true
}
