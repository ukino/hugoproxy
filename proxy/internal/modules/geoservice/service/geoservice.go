package service

import (
	"fmt"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/components"
	"go.uber.org/zap"
	"net/http"
	"os"
)

type AddressService struct {
	logger *zap.Logger
}

func NewAddressService(components *components.Components) *AddressService {
	return &AddressService{logger: components.Logger}
}

func (a *AddressService) CreateGeocodeRequest(r *http.Request) (*http.Request, error) {
	req, err := http.NewRequest(http.MethodPost, "https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address", r.Body)
	return req, err
}

func (a *AddressService) CreateGeocodeClient(req *http.Request) *http.Client {
	client := &http.Client{}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Token %s", os.Getenv("DADATA_API_KEY")))
	return client
}

func (a *AddressService) DoRequest(client *http.Client, req *http.Request) (*http.Response, error) {
	resp, err := client.Do(req)
	return resp, err
}

func (a *AddressService) CreateSearchRequest(r *http.Request) (*http.Request, error) {
	req, err := http.NewRequest(http.MethodPost, "https://suggestions.dadata.ru/suggestions/api/4_1/rs/iplocate/address", r.Body)
	return req, err
}

func (a *AddressService) CreateSearchClient(req *http.Request) *http.Client {
	client := &http.Client{}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Token %s", os.Getenv("DADATA_API_KEY")))
	return client
}
