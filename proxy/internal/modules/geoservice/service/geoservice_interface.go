package service

import "net/http"

type GeoServicer interface {
	CreateGeocodeRequest(r *http.Request) (*http.Request, error)
	CreateGeocodeClient(req *http.Request) *http.Client
	DoRequest(client *http.Client, req *http.Request) (*http.Response, error)
	CreateSearchRequest(r *http.Request) (*http.Request, error)
	CreateSearchClient(req *http.Request) *http.Client
}
