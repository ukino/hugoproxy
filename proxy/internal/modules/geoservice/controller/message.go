package controller

type GeocodeRequest struct {
	Lat string `json:"lat"`
	Lng string `json:"lon"`
}

type GeocodeResponse struct {
	Suggestions []*Suggestion `json:"suggestions"`
}

type SearchRequest struct {
	Query string `json:"ip"`
}

type SearchResponse struct {
	Suggestion *Suggestion `json:"location"`
}

type Suggestion struct {
	Value             string             `json:"value"`
	UnrestrictedValue string             `json:"unrestricted_value"`
	Data              map[string]*string `json:"data"`
}
