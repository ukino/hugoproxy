package controller

import (
	"encoding/json"
	"github.com/ptflp/godecoder"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/components"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/responder"
	"gitlab.com/ukino/hugoproxy/proxy/internal/modules/geoservice/service"
	"io"
	"net/http"
)

type GeoServicer interface {
	GeocodeHandler(w http.ResponseWriter, r *http.Request)
	SearchHandler(w http.ResponseWriter, r *http.Request)
}

type Address struct {
	service.GeoServicer
	responder.Responder
	godecoder.Decoder
}

func NewAddress(service service.GeoServicer, components *components.Components) GeoServicer {
	return &Address{
		GeoServicer: service,
		Responder:   components.Responder,
		Decoder:     components.Decoder,
	}
}

func (a *Address) GeocodeHandler(w http.ResponseWriter, r *http.Request) {
	req, err := a.CreateGeocodeRequest(r)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	client := a.CreateGeocodeClient(req)
	resp, err := a.DoRequest(client, req)
	defer resp.Body.Close()
	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		a.ErrorForbidden(w, err)
	}
	var GeoResp GeocodeResponse
	err = json.Unmarshal(bodyText, &GeoResp)
	if err != nil {
		a.ErrorBadRequest(w, err)
	}
	a.Responder.OutputJSON(w, GeoResp)
}

func (a *Address) SearchHandler(w http.ResponseWriter, r *http.Request) {
	req, err := a.CreateSearchRequest(r)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	client := a.CreateSearchClient(req)
	resp, err := a.DoRequest(client, req)
	defer resp.Body.Close()
	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		a.Responder.ErrorForbidden(w, err)
		return
	}
	var SearchResp SearchResponse
	err = json.Unmarshal(bodyText, &SearchResp)
	if err != nil {
		a.Responder.ErrorBadRequest(w, err)
		return
	}
	a.Responder.OutputJSON(w, SearchResp)
}
