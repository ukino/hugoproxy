package controller

import (
	"github.com/go-chi/jwtauth/v5"
	"github.com/ptflp/godecoder"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/components"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/responder"
	"gitlab.com/ukino/hugoproxy/proxy/internal/modules/geoservice/service"
	"go.uber.org/zap"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGeocodeHandler(t *testing.T) {
	token := jwtauth.New("HS256", []byte("secret"), nil)
	users := make(map[int]string)
	logger := zap.NewExample()
	decoder := godecoder.NewDecoder()
	Responder := responder.NewResponder(decoder, logger)
	component := components.NewComponents(Responder, decoder, logger, token, users)

	address := NewAddress(service.NewAddressService(component), component)

	request, err := http.NewRequest("GET", "http://localhost:8080/api/address/geocode", nil)
	if err != nil {
		return
	}

	rr := httptest.NewRecorder()

	handlerFunc := http.HandlerFunc(address.GeocodeHandler)

	handlerFunc.ServeHTTP(rr, request)
}

func TestSearchHandler(t *testing.T) {
	token := jwtauth.New("HS256", []byte("secret"), nil)
	users := make(map[int]string)
	logger := zap.NewExample()
	decoder := godecoder.NewDecoder()
	Responder := responder.NewResponder(decoder, logger)
	component := components.NewComponents(Responder, decoder, logger, token, users)

	address := NewAddress(service.NewAddressService(component), component)
	request, err := http.NewRequest("GET", "http://localhost:8080/api/address/search", nil)
	if err != nil {
		return
	}

	rr := httptest.NewRecorder()

	handlerFunc := http.HandlerFunc(address.SearchHandler)

	handlerFunc.ServeHTTP(rr, request)
}
