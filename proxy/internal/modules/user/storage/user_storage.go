package storage

import (
	"context"
	"gitlab.com/ukino/hugoproxy/proxy/internal/db/adapter"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/db/scanner"
	"gitlab.com/ukino/hugoproxy/proxy/internal/models"
)

type UserStorage struct {
	adapter *adapter.SQLAdapter
}

func NewUserStorage(sqlAdapter *adapter.SQLAdapter) *UserStorage {
	return &UserStorage{adapter: sqlAdapter}
}

func (u *UserStorage) Create(ctx context.Context, user models.User) error {
	err := u.adapter.Create(ctx, &user)
	return err
}

func (u *UserStorage) GetByID(ctx context.Context, id int) (models.User, error) {
	var entity models.User
	var dest []models.User
	err := u.adapter.List(ctx, &dest, entity.TableName(), adapter.Condition{
		Equal: map[string]interface{}{"id": id},
	})
	if err != nil {
		return models.User{}, err
	}
	if len(dest) < 1 {
		return models.User{}, err
	}
	return dest[0], err
}

func (u *UserStorage) Update(ctx context.Context, user models.User) error {
	err := u.adapter.Update(ctx, &user, adapter.Condition{Equal: map[string]interface{}{"id": user.ID}}, scanner.Update)
	if err != nil {
		return err
	}
	return nil
}

func (u *UserStorage) Delete(ctx context.Context, id int) error {
	byID, err := u.GetByID(ctx, id)
	if err != nil {
		return err
	}

	byID.IsDeleted = true

	err = u.Update(ctx, byID)
	if err != nil {
		return err
	}

	return nil
}

func (u *UserStorage) List(ctx context.Context, c adapter.Condition) ([]models.User, error) {
	var dest models.User
	var result []models.User
	err := u.adapter.List(ctx, &result, dest.TableName(), c)
	return result, err
}
