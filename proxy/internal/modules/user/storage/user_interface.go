package storage

import (
	"context"
	"gitlab.com/ukino/hugoproxy/proxy/internal/db/adapter"
	"gitlab.com/ukino/hugoproxy/proxy/internal/models"
)

type UserRepository interface {
	Create(ctx context.Context, user models.User) error
	GetByID(ctx context.Context, id int) (models.User, error)
	Update(ctx context.Context, user models.User) error
	Delete(ctx context.Context, id int) error
	List(ctx context.Context, c adapter.Condition) ([]models.User, error)
}
