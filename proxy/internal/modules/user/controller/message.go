package controller

type GetUserRequest struct {
	Id int `json:"id"`
}

type GetUserResponse struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Password  string `json:"password"`
	IsDeleted bool   `json:"is_deleted"`
}

type AddUserRequest struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Password  string `json:"password"`
	IsDeleted bool   `json:"is_deleted"`
}

type AddUserResponse struct {
	Successful bool `json:"successful"`
}

type UpdateUserRequest struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	Password  string `json:"password"`
	IsDeleted bool   `json:"is_deleted"`
}

type UpdateUserResponse struct {
	Successful bool `json:"successful"`
}

type DeleteUserRequest struct {
	Id int `json:"id"`
}

type DeleteUserResponse struct {
	Successful bool `json:"successful"`
}

type ListUserRequest struct {
	Equal    map[string]interface{} `json:"equal"`
	NotEqual map[string]interface{} `json:"not_equal"`
	Order    []*struct {
		Field string `json:"field"`
		Asc   bool   `json:"asc"`
	} `json:"order"`
	LimitOffset *struct {
		Offset int64 `json:"offset"`
		Limit  int64 `json:"limit"`
	} `json:"limit_offset"`
	ForUpdate bool `json:"for_update"`
	Upsert    bool `json:"upsert"`
}

type ListUserResponse struct {
	List []struct {
		ID        int    `json:"id"`
		Name      string `json:"name"`
		Password  string `json:"password"`
		IsDeleted bool   `json:"is_deleted"`
	} `json:"list"`
}
