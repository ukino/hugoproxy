package controller

import (
	"context"
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"github.com/ptflp/godecoder"
	"gitlab.com/ukino/hugoproxy/proxy/internal/db/adapter"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/components"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/responder"
	"gitlab.com/ukino/hugoproxy/proxy/internal/models"
	"gitlab.com/ukino/hugoproxy/proxy/internal/modules/user/service"
	"log"
	"net/http"
	"strconv"
)

type UserRepository interface {
	AddUserHandler(w http.ResponseWriter, r *http.Request)
	GetUserHandler(w http.ResponseWriter, r *http.Request)
	UpdateUserHandler(w http.ResponseWriter, r *http.Request)
	DeleteUserHandler(w http.ResponseWriter, r *http.Request)
	ListUserHandler(w http.ResponseWriter, r *http.Request)
}

type UserServiceStruct struct {
	service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUserServiceStruct(service service.Userer, components *components.Components) UserRepository {
	return &UserServiceStruct{
		Userer:    service,
		Responder: components.Responder,
		Decoder:   components.Decoder,
	}
}

func (u *UserServiceStruct) GetUserHandler(w http.ResponseWriter, r *http.Request) {
	var GUR GetUserRequest
	urlId, _ := strconv.Atoi(chi.URLParam(r, "id"))
	GUR.Id = urlId
	id, err := u.Userer.GetByID(context.Background(), GUR.Id)
	if err != nil {
		u.Responder.ErrorBadRequest(w, err)
		return
	}
	response := GetUserResponse{
		ID:        id.ID,
		Name:      id.Name,
		Password:  id.Password,
		IsDeleted: id.IsDeleted,
	}
	u.Responder.OutputJSON(w, response)
}

func (u *UserServiceStruct) AddUserHandler(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		u.Responder.ErrorBadRequest(w, err)
		return
	}
	err = u.Userer.AddUser(context.Background(), user)
	if err != nil {
		u.Responder.ErrorBadRequest(w, err)
		return
	}
	response := AddUserResponse{Successful: true}
	u.Responder.OutputJSON(w, response)
}

func (u *UserServiceStruct) UpdateUserHandler(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		u.Responder.ErrorBadRequest(w, err)
		return
	}
	log.Println(user)
	err = u.Userer.UpdateUser(context.Background(), user)
	if err != nil {
		u.Responder.ErrorBadRequest(w, err)
		return
	}
	response := UpdateUserResponse{Successful: true}
	u.Responder.OutputJSON(w, response)
}

func (u *UserServiceStruct) DeleteUserHandler(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		u.Responder.ErrorBadRequest(w, err)
		return
	}
	err = u.Userer.DeleteUser(context.Background(), user.ID)
	if err != nil {
		u.Responder.ErrorBadRequest(w, err)
		return
	}
	response := DeleteUserResponse{Successful: true}
	u.Responder.OutputJSON(w, response)
}

func (u *UserServiceStruct) ListUserHandler(w http.ResponseWriter, r *http.Request) {
	var cond adapter.Condition
	err := json.NewDecoder(r.Body).Decode(&cond)
	if err != nil {
		u.Responder.ErrorBadRequest(w, err)
		return
	}
	users, err := u.Userer.ListOfUsers(context.Background(), cond)
	if err != nil {
		u.Responder.ErrorBadRequest(w, err)
		return
	}
	u.Responder.OutputJSON(w, users)
}
