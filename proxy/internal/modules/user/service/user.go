package service

import (
	"context"
	"gitlab.com/ukino/hugoproxy/proxy/internal/db/adapter"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/components"
	"gitlab.com/ukino/hugoproxy/proxy/internal/models"
	"gitlab.com/ukino/hugoproxy/proxy/internal/modules/user/storage"
	"go.uber.org/zap"
)

type UserService struct {
	storage storage.UserRepository
	logger  *zap.Logger
}

func NewUserService(storage storage.UserRepository, components *components.Components) *UserService {
	return &UserService{
		storage: storage,
		logger:  components.Logger,
	}
}

func (u *UserService) GetByID(ctx context.Context, id int) (models.User, error) {
	byID, err := u.storage.GetByID(ctx, id)
	return byID, err
}

func (u *UserService) AddUser(ctx context.Context, user models.User) error {
	err := u.storage.Create(ctx, user)
	return err
}

func (u *UserService) UpdateUser(ctx context.Context, user models.User) error {
	err := u.storage.Update(ctx, user)
	return err
}

func (u *UserService) DeleteUser(ctx context.Context, id int) error {
	err := u.storage.Delete(ctx, id)
	return err
}

func (u *UserService) ListOfUsers(ctx context.Context, c adapter.Condition) ([]models.User, error) {
	list, err := u.storage.List(ctx, c)
	return list, err
}
