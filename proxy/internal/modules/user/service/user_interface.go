package service

import (
	"context"
	"gitlab.com/ukino/hugoproxy/proxy/internal/db/adapter"
	"gitlab.com/ukino/hugoproxy/proxy/internal/models"
)

type Userer interface {
	AddUser(ctx context.Context, user models.User) error
	GetByID(ctx context.Context, id int) (models.User, error)
	UpdateUser(ctx context.Context, user models.User) error
	DeleteUser(ctx context.Context, id int) error
	ListOfUsers(ctx context.Context, c adapter.Condition) ([]models.User, error)
}
