package modules

import (
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/components"
	auth "gitlab.com/ukino/hugoproxy/proxy/internal/modules/auth/controller"
	geo "gitlab.com/ukino/hugoproxy/proxy/internal/modules/geoservice/controller"
	user "gitlab.com/ukino/hugoproxy/proxy/internal/modules/user/controller"
)

type Controllers struct {
	Geo  geo.GeoServicer
	Auth auth.Auther
	User user.UserRepository
}

func NewControllers(services *Services, components *components.Components) *Controllers {
	return &Controllers{
		Geo:  geo.NewAddress(services.Geo, components),
		Auth: auth.NewAuth(services.Auth, components),
		User: user.NewUserServiceStruct(services.User, components),
	}
}
