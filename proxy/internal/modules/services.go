package modules

import (
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/components"
	auth "gitlab.com/ukino/hugoproxy/proxy/internal/modules/auth/service"
	geo "gitlab.com/ukino/hugoproxy/proxy/internal/modules/geoservice/service"
	user "gitlab.com/ukino/hugoproxy/proxy/internal/modules/user/service"
	"gitlab.com/ukino/hugoproxy/proxy/internal/storages"
)

type Services struct {
	Geo  geo.GeoServicer
	Auth auth.Auther
	User user.Userer
}

func NewServices(storages *storages.Storages, components *components.Components) *Services {
	return &Services{
		Geo:  geo.NewAddressService(components),
		Auth: auth.NewAuthService(components),
		User: user.NewUserService(storages.User, components),
	}
}
