package storages

import (
	"gitlab.com/ukino/hugoproxy/proxy/internal/db/adapter"
	user "gitlab.com/ukino/hugoproxy/proxy/internal/modules/user/storage"
)

type Storages struct {
	User user.UserRepository
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		User: user.NewUserStorage(sqlAdapter),
	}
}
