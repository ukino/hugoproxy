package router

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"github.com/ptflp/godecoder"
	"gitlab.com/ukino/hugoproxy/proxy/config"
	"gitlab.com/ukino/hugoproxy/proxy/internal/db"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/components"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/db/migrate"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/db/scanner"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/middleware"
	"gitlab.com/ukino/hugoproxy/proxy/internal/infrastructure/responder"
	"gitlab.com/ukino/hugoproxy/proxy/internal/models"
	"gitlab.com/ukino/hugoproxy/proxy/internal/modules"
	"gitlab.com/ukino/hugoproxy/proxy/internal/storages"
	"go.uber.org/zap"
	"log"
	"net/http"
)

func InitComponents() *components.Components {
	token := jwtauth.New("HS256", []byte("secret"), nil)
	users := make(map[int]string)
	logger := zap.NewExample()
	decoder := godecoder.NewDecoder()
	Responder := responder.NewResponder(decoder, logger)
	component := components.NewComponents(Responder, decoder, logger, token, users)
	return component
}

func NewRouter() http.Handler {
	r := chi.NewRouter()

	proxy := middleware.NewReverseProxy("hugo", "1313")

	r.Use(proxy.ReverseProxy)

	component := InitComponents()
	conf := config.NewAppConf()
	tableScanner := scanner.NewTableScanner()
	tableScanner.RegisterTable(&models.User{})
	db, sqlAdapter, err := db.NewSqlDB(conf.DB, tableScanner, component.Logger)
	if err != nil {
		log.Fatal(err)
	}

	migrator := migrate.NewMigrator(db, conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		log.Println(err)
	}

	Storages := storages.NewStorages(sqlAdapter)
	Services := modules.NewServices(Storages, component)
	controllers := modules.NewControllers(Services, component)

	r.Get("/swagger", swaggerUI)
	r.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	})

	r.Route("/api", func(r chi.Router) {
		r.Get("/*", middleware.Handler)
		r.Post("/login", controllers.Auth.LoginHandler)
		r.Post("/register", controllers.Auth.RegisterHandler)

		r.Route("/users", func(r chi.Router) {
			r.Use(jwtauth.Verifier(component.TokenAuth))
			r.Use(jwtauth.Authenticator)

			r.Get("/{id}", controllers.User.GetUserHandler)
			r.Post("/add", controllers.User.AddUserHandler)
			r.Post("/update", controllers.User.UpdateUserHandler)
			r.Post("/delete", controllers.User.DeleteUserHandler)
			r.Get("/list", controllers.User.ListUserHandler)
		})

		r.Route("/address", func(r chi.Router) {
			r.Use(jwtauth.Verifier(component.TokenAuth))
			r.Use(jwtauth.Authenticator)

			r.Post("/search", controllers.Geo.SearchHandler)
			r.Post("/geocode", controllers.Geo.GeocodeHandler)
		})
	})
	return r
}
